# Utilisation de Mobilizon

---

## Comment utiliser Mobilizon ?

Avant tout : merci d'utiliser Mobilizon !
Cette documentation a pour objectif de vous aider dans les différentes options et fonctionnalités.

### Utilisateur et utilisatrice

  * [Comment créer un compte](utilisateur et utilisatrice/creation-compte.md)
  * [Comment créer un profil](utilisateur et utilisatrice/creation-profile.md)
  * [Les paramètres de votre compte](utilisateur et utilisatrice/parametres-compte.md)

### Événements

  * [Comment commenter un événement](evenements/commentaires-evenement.md)
  * [Comment créer un événement](evenements/creation-evenement.md)
  * [Partager, signaler, éditer un événement](evenements/actions-evenement.md)
